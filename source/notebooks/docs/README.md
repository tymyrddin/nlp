# Analysing documents and text

Amazon Comprehend is a text analytics service: extracting key phrases and entities, language detection, topic modeling, 
sentiment analysis and syntax analysis. 

Using Amazon Comprehend to examine texts to determine their primary language, extracting information such as entities 
(people or places), key phrases (noun phrases that are indicative of the content), emotional sentiments, and topics 
from a set of documents.

- [Detecting the dominant language in a text](language.ipynb)
- [Detecting the dominant language in multiple documents](docs_language.ipynb)
- [Detecting named entities in a text](named_entities.ipynb)
- [Detect entities in a set of documents](docs_entities.ipynb)
- [Detecting key phrases](key_phrases.ipynb)
- [Sentiment analysis](sentiment_analysis.ipynb)
- [Lambda trigger function for S3](lambda_s3.ipynb)