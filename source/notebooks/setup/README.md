# Set up AI and ML AWS services

There are many cloud providers. Per the [Canalys analysis](https://canalys-prod-public.s3.eu-west-1.amazonaws.com/static/press_release/2022/1504763491cloud_pr_Q4_2021.pdf), as of Q4 2021, AWS is still the top vendor, owning more than a third of the overall public cloud infrastructure market.

Amazon offers a very large array of cloud services. One of their competitive advantages is exactly that: a very broad and deep cloud computing ecosystem. 
In the area of ML, Amazon has thousands of use cases, and a professed goal of every imaginable ML service being provided on AWS. 
This is why we focus on doing ML on AWS for our first experiences. We can explore other services such as Qwiklabs later.

- [Creating and using an S3 Bucket](s3.md)
- [Configuring and using the AWS CLI](cli.md)
- [Using the AWS management console](services.md)
