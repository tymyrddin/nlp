# Using the AWS management console

- [x] Go to the [AWS Management Console](https://console.aws.amazon.com/console/) and up top left, click services

![Management console](assets/services_ml.png)

- [x] Explore [Amazon Lex](https://console.aws.amazon.com/lex)
- [x] Explore [Amazon Comprehend](https://console.aws.amazon.com/comprehend)