# Configuring and using the CLI

- [x] Go to the AWS Management Console and choose IAM:

![Management console](assets/management_console_IAM.png)

- [x] Click on _Users_ in the left navigation bar and in the next screen in the upper right corner, just under your account name, select _Security Credentials_.

![Choose security credentials](assets/choose_security_credentials.png)

- [x] Click the Access keys (access key ID and secret access key) option in the Security Credentials screen

![Choose access keys](assets/access_keys.png)

- [x] Click _Create New Access Key_

![Choose access keys](assets/create_new_access_key.png)

- [x] Download the key file. 
 
![Choose access keys](assets/access_key_created.png)

- [x] The rootkey.csv file that contains the keys will be downloaded. You can view the details by opening the file. Store the keys in a safe location. Protect your AWS account and never share, email, or store keys in a non-secure location. An AWS representative will never request your keys, so be vigilant when it comes to potential phishing scams.
- [x] [Install and configure AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- [x] Test access works with:

```shell
$ aws s3 cp s3://bucket-name . --recursive
```
- [x] Check `.aws/config` and `.aws/credentials` and make corrections if need be
- [x] Browse [CLI command reference](https://docs.aws.amazon.com/cli/latest/reference/)