# Natural Language Processing

Basic use of NLP techniques.

### Learning

- [x] [Set up AI and ML AWS services](source/notebooks/setup)
- [x] [Analysing documents and text](source/notebooks/docs)

...

## Requirements

Anaconda3 with python 3.9 interpreter

## Installation

Fork or download, [install anaconda](https://www.anaconda.com/products/individual), and in conda virtual environment for the project, run:

```bash
$ conda env create -f environment.yml
```
After updating packages (and subsequently running the tests):

```bash
$ conda env export > environment.yml
```

## Code quality

We automated the code formatting with [black-jupyter](https://anaconda.org/conda-forge/black-jupyter) and PEP8 compliance with [flake8_nb](https://anaconda.org/conda-forge/flake8-nb) processes
by using the [pre-commit framework](https://anaconda.org/conda-forge/pre_commit) and its [hooks](https://anaconda.org/conda-forge/pre-commit-hooks).
It runs a short script before committing. If the script passes, then the commit is made, else, the commit is denied.

Running Black Jupyter on all notebooks from the root of the repo:

```bash
$ black .
```

Running Flake8 NB on all notebooks from the root of the repo:

```bash
$ flake8_nb source/notebooks
```

If you wish to additionally use type annotations and [mypy](https://anaconda.org/anaconda/mypy), install [data-science-types](https://anaconda.org/conda-forge/data-science-types) for libraries like matplotlib, numpy and pandas that do not have type information, and install [nbqa](https://anaconda.org/conda-forge/nbqa).

Run on all notebooks in the root directory of the repo with:

```bash
$ nbqa mypy .
```

Run on a specific `notebook.ipynb` with:

```bash
$ nbqa mypy notebook.ipynb
```

## Testing

* Write unit tests for Lambda functions.
* Determine end-to-end tests to be run against AWS Step Function state machine.

## Security

We run [Snyk scans](https://app.snyk.io/).

## Project status

Empowered by Life.

## Contributing

This project welcomes contributions.

## License

Unlicensed. Or Universal Licensed. Whatever. This is in our playground.
